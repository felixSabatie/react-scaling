class IoController < ApplicationController
  def index
    file = File.read("#{Rails.root}/io_test_file.txt")
    render plain: file
  end
end
